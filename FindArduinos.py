import time,serial,yaml

from logger_config import get_logger

logger = get_logger(__name__)

def find_arduino_ports():

    with open('../conf/ports.yaml', encoding='UTF8') as f:
        ports = yaml.load(f,Loader=yaml.FullLoader)
        for arduino_port in ports['serial_candidate']:
            try:
                serial_io = serial.Serial(arduino_port, baudrate=57600, parity='N', stopbits=1, bytesize=8, timeout=8)
                time.sleep(2)
                serial_io.write(f"device-info".encode())
                time.sleep(0.5)
                message = serial_io.readline()
                message = message.decode('utf-8').strip()
                if message.startswith("diffdrive,"):
                    ports['diffdrive_serial'] = arduino_port
                    logger.info(f"diff drive is {arduino_port}")
                elif message.startswith("jet,"):
                    ports['jet_serial'] = arduino_port
                    logger.info(f"jet is {arduino_port}")
            except:
                logger.info(f"{arduino_port} is unavailable.")
            f.close()

    with open('../conf/ports.yaml', 'w', encoding='UTF8') as f:
        yaml.dump(ports,f)
        f.close()


if __name__ == "__main__":
    find_arduino_ports()
