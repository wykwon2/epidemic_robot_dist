import time
from LidarPacketHandler import LidarPacketHandler
from logger_config import get_logger

logger = get_logger(__name__)

class LidarController:
    x_lower = 0
    x_upper = 200
    y_lower = -220
    y_upper = 220
    lidar_running=False

    def __init__(self, serial_port, baudrate='57600', simulation=False):
        self.simulation=simulation
        if not self.simulation:
            try:
                self.packet_handler = LidarPacketHandler(serial_port, baudrate)
            except:
                logger.warning("lidar 초기화 실패")
                self.packet_handler=None



    def set_obstruction_area(self, front_range, side_range):
        self.x_lower = 0
        self.x_upper = front_range
        self.y_upper = side_range
        self.y_lower = -side_range

    def get_obstruction_area(self):
        return (self.x_upper, self.y_upper)

    def detect_obstruction(self):
        """주행전방의 장애물을 감지한다. """
        obstruction_point_cnt= 0
        try:
            if not self.simulation:
                data = self.packet_handler.read_2d_data()
            for point in data:
                x = point[2]
                y = point[3]
                if x > self.x_lower and x < self.x_upper and y > self.y_lower and y < self.y_upper:
                    obstruction_point_cnt+=1
            return obstruction_point_cnt
        except:
            logger.warning("lidar 초기화 실패")
            return 0


    def start_sensing(self):
        if not self.simulation:
            try:
                self.packet_handler.request_2d_mode()
                self.lidar_running=True
            except:
                self.packet_handler = None
                logger.warning("lidar 초기화 실패")
        time.sleep(0.2)

    def stop_sensing(self):
        if not self.simulation and self.lidar_running:
            try:
                self.packet_handler.stop()
                self.self.lidar_running=False
            except:
                logger.warning("lidar 초기화 실패")
                self.packet_handler=None

if __name__ == "__main__":
    lidar = LidarController(serial_port="COM6", baudrate="57600")
#    lidar = LidarController(serial_port="/dev/ttyAMA0", baudrate="57600")
    lidar.set_obstruction_area(front_range=300,side_range=300)

    lidar.start_sensing()
    for i in range(0, 200):
        if lidar.detect_obstruction():
            logger.debug(f'{i}th obstruction detected.')

    lidar.stop_sensing()
