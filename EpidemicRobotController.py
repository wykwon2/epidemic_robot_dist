import time, requests
from typing import Optional

import serial

import ConfigManager
from DiffDriveController import DiffDriveController
from JetTurretController import JetTurretController
from MarkerDetector import MarkerDetector
from SingleCamera import SingleCamera
from dataclass import MarkerPose, DriveStatus, MarkerActionInfo, NavigationStatus, NavigationParam, ERROR, TASK_STATE
from logger_config import get_logger
from user_exceptions import NoMarkerException, MarkerServerException, CmdConflictException, StopByUserException

logger = get_logger(__name__)


class EpidemicRobotController:
    drive_serial_port: str
    lidar_serial_port: str
    turret_serial_port: str
    # 목표 마커, 따라갈 대상
    target_marker: Optional[MarkerPose] = None
    # 작업 마커, 작업할 마커
    marker_action: Optional[MarkerActionInfo] = None
    drive_status = None
    task_running = False
    cmd_stop_all = False

    def __init__(self, config: ConfigManager, simulation=False):
        self.config = config
        self.navigation_param = config.navigation_params
        self.lidar_serial_port=config.serial_ports['lidar_serial']
        if not simulation:
            self._find_serial_port()
        else:
            self.drive_serial_port="dummy_serial"
            self.turret_serial_port="dummy_serial"

        self.diff_drive = DiffDriveController(drive_serial_port=self.drive_serial_port, simulation=simulation,
                                              lidar_serial_port=self.lidar_serial_port, param=config.drive_params)
        self.jet_turret = JetTurretController(serial_port=self.turret_serial_port,
                                              param=config.turret_params, simulation=simulation)
        camera = SingleCamera()
        camera.start()
        self.marker_detector = MarkerDetector(camera=camera, config=config)

    def _find_serial_port(self):
        found_ports = {}
        for arduino_port in self.config.serial_ports['serial_candidate']:
            try:
                serial_io = serial.Serial(arduino_port, baudrate=57600, parity='N', stopbits=1, bytesize=8, timeout=8)
                time.sleep(2)
                serial_io.write(f"device-info".encode())
                time.sleep(0.5)
                message = serial_io.readline()
                message = message.decode('utf-8').strip()
                if message.startswith('diffdrive'):
                    found_ports['diffdrive'] = arduino_port
                elif message.startswith('jet'):
                    found_ports['jet'] = arduino_port

                serial_io.close()
                if len(found_ports) >= 2:
                    self.drive_serial_port = found_ports['diffdrive']
                    self.turret_serial_port = found_ports['jet']
            except:
                print(f"{arduino_port} is unavailable.")

        # self.drive_serial_port = 'COM4'
        # self.lidar_serial_port = self.config.serial_ports['lidar_serial']
        # self.turret_serial_port = 'COM6'

    def stop_all(self):
        self.cmd_stop_all = True
        self.diff_drive.stop_all()

    def set_navigation_param(self, navigation_param: NavigationParam):
        self.navigation_param = navigation_param

    def export_marker_pose(self):
        marker_pose: MarkerPose = self.marker_detector.get_nearest_marker_pose()
        addr = self.config.server_ports['ip_addr']
        port = self.config.server_ports['rest_port']
        serial = self.config.robot_serial
        url = f'http://{addr}:{port}/set-marker-pose/{serial}'
        logger.debug(url)
        if marker_pose:
            requests.post(url, data=marker_pose.model_dump_json())
        else:
            requests.post(f'http://{addr}:{port}/set-empty-marker-pose/{serial}')
        logger.debug(f'export marker pose {marker_pose}')

    async def subtask_marker(self, start_time: float, status_msg: str):
        ''' 마커인식과 관련된 예외처리와 메시지 전달하는 경우에 사용하는 함수'''
        logger.debug('subtask_marker')
        for i in range(0, 10):
            yield self.write_navigation_status(time=time.time() - start_time, status=status_msg,
                                               msg='마커인식 준비', marker_pose=self.target_marker)
            time.sleep(0.1)

        self.target_marker = self.detect_marker()
        logger.debug('detect marker')
        if self.target_marker :
            yield self.write_navigation_status(time=time.time() - start_time, status=status_msg,
                                               msg='마커가 인식되었습니다', marker_pose=self.target_marker)
        else:
            # 여기서 메시지를 error로 바꾸어주어야 함
            yield self.write_navigation_status(time=time.time() - start_time, status=ERROR.marker,
                                               msg='마커가 인식되지 않았습니다')
            raise NoMarkerException()
        logger.debug('subtask_marker end')

    async def subtask_turn(self, turn_angle: float, start_time: float, status: str, msg: str):
        async for drive_status in self.diff_drive.turn(turn_angle):
            self.drive_status = drive_status
            yield self.write_navigation_status(time=time.time() - start_time, status=status,
                                               msg=msg, marker_pose=self.target_marker,
                                               drive_status=self.drive_status)

    async def subtask_move(self, dist: float, start_time: float, status: str, msg: str):
        async for drive_status in self.diff_drive.move(dist):
            self.drive_status = drive_status
            yield self.write_navigation_status(time=time.time() - start_time, status=status,
                                               msg=msg, marker_pose=self.target_marker,
                                               drive_status=drive_status)

    async def batch_task(self):
        if self.task_running:
            yield self.write_navigation_status(time=0, status='error',
                                               error=ERROR.conflict.value, msg=f'방역일괄작업 중복실행')
            raise CmdConflictException()

        start_time = time.time()
        try:
            while True:
                yield self.write_navigation_status(time=0, status='소독중', msg=f'일괄작업 시작')

                # error가 나오지 않을때까지 반복해서 탐색 작업함
                while not self.target_marker:
                    async for event in self.search_marker(start_time=start_time):
                        yield event

                async for event in self.approach_marker(start_time=start_time):
                    yield event

                async for event in self.align_marker(start_time=start_time, ):
                    yield event

                async for event in self.spray_marker(self.marker_action.marker_id, start_time=start_time):
                    yield event

                if self.marker_action.marker_id == 0:
                    yield self.write_navigation_status(time=time.time() - start_time, status='소독완료',
                                                       msg=f'방역작업 종료')
                    return


        # except MarkerServerException as e:
        #     logger.error(e)
        #     yield self.write_navigation_status(time=time.time() - start_time, status='error',
        #                                        error=ERROR.network, msg=f'{e}')
        #
        # except NoMarkerException as e:
        #     logger.error(e)
        #     yield self.write_navigation_status(time=time.time() - start_time, status='error',
        #                                        error=ERROR.marker,
        #                                        msg=f'{e}')
        # except StopByUserException as e:
        #     logger.error(e)
        #     yield self.write_navigation_status(time=time.time() - start_time, status=ERROR.stop,
        #                                        error=ERROR.stop,
        #                                        msg=f'{e}', marker_pose=self.target_marker,
        #                                        drive_status=self.drive_status)
        except Exception as e:
            logger.error(e)
            # yield self.write_navigation_status(time=time.time() - start_time, status=self.drive_status.error,
            #                                    error=self.drive_status.error,
            #                                    msg=f'{e}', marker_pose=self.target_marker,
            #                                    drive_status=self.drive_status)
        finally:
            self.cmd_stop_all = False
            self.task_running = False

    async def spray_marker(self, marker_id: int, start_time, as_subtask=True):
        subtask_name = '소독액분사'
        marker_action_info = self.jet_turret.get_marker_action(marker_id)
        wait_time = self.jet_turret.param.jet_wait_time
        if marker_action_info.wait:
            marker_msg = f'{marker_id}:{marker_action_info.horizontal}-' \
                         f'{marker_action_info.vertical}-{marker_action_info.fan_speed}-' \
                         f'{wait_time}초 대기'
        else:
            marker_msg = f'{marker_id}:{marker_action_info.horizontal}-' \
                         f'{marker_action_info.vertical}-{marker_action_info.fan_speed}-'

        yield self.write_navigation_status(time=time.time() - start_time, status=subtask_name,
                                           msg=f'{marker_msg}')
        try:
            self.jet_turret.marker_action(marker_id)

        except Exception as e:
            logger.error(e)
            if as_subtask:
                yield self.write_navigation_status(time=time.time() - start_time, status='error',
                                               error=ERROR.unknown.value, msg=f'{e}')
                raise e
        else:
            yield self.write_navigation_status(time=time.time() - start_time, status=subtask_name,
                                               msg=f'분사작업 종료')


    async def approach_marker(self, start_time=None, as_subtask=True):
        '''마커를 감지하고 마커가 정면에 오도록 하고, 마커에 step만큼 접근하는 동작을 수행함
            1. 마커인식
            2. 마커가 전면에 오도록 turn
            3. 마커를 상대로 step만큼 전진하는데

            :return True: 마커에 근접(min_dist=60cm 이내에 마커가 있음), False: 마커와 거리가 먼 상태

        '''
        subtask_name = '마커접근작업'
        if self.task_running:
            yield self.write_navigation_status(time=0, status='error',
                                               error=ERROR.conflict.value, msg=f'마커접근작업 중복실행')
            raise CmdConflictException()

        self.task_running = True
        if start_time == None:
            start_time = time.time()

        try:
            while True:
                self.check_stop_all()
                async for event in self.subtask_marker(start_time, status_msg=subtask_name):
                    yield event

                self.check_stop_all()
                async for event in self.subtask_turn(self.target_marker.theta, start_time=start_time,
                                                     status=subtask_name, msg='마커가 정면에 오도록 회전'):
                    yield event

                self.check_stop_all()
                async for event in self.subtask_marker(start_time, status_msg=subtask_name):
                    yield event

                self.check_stop_all()
                if self.target_marker.dist == 'far':
                    async for event in self.subtask_move(dist=self.navigation_param.delta, start_time=start_time,
                                                         status=subtask_name, msg=f'먼거리에서 마커로 접근'):
                        yield event

                elif self.target_marker.dist == 'near':
                    target_dist = self.target_marker.r - self.navigation_param.min_marker_dist - 0.5 * self.navigation_param.epsilon
                    async for event in self.subtask_move(dist=target_dist, start_time=start_time,
                                                         status=subtask_name, msg=f'가까운 거리에서 마커로 접근'):
                        yield event

                elif self.target_marker.dist == 'close':
                    break


        except MarkerServerException as e:
            logger.error(e)
            if as_subtask:
                yield self.write_navigation_status(time=time.time() - start_time, status='error',
                                               error=ERROR.network.value, msg=f'{e}')
                raise e

        except NoMarkerException as e:
            logger.error(e)
            if as_subtask:
                yield self.write_navigation_status(time=time.time() - start_time, status='error',
                                               error=ERROR.marker, msg=f'{e}')
                raise e

        except StopByUserException as e:
            logger.error(e)
            if as_subtask:
                yield self.write_navigation_status(time=time.time() - start_time, status=ERROR.stop,
                                                   error=ERROR.stop, msg=f'{e}', marker_pose=self.target_marker,
                                                   drive_status=self.drive_status)
                raise e

        except Exception as e:
            logger.error(e)
            if as_subtask:
                yield self.write_navigation_status(time=time.time() - start_time, status=self.drive_status.error,
                                                   error=self.drive_status.error, msg=f'{e}',
                                                   marker_pose=self.target_marker, drive_status=self.drive_status)
                raise e
        else:
            if not as_subtask:
                subtask_name = TASK_STATE.action_complete
            yield self.write_navigation_status(time=time.time() - start_time, status=subtask_name,
                                               msg=f'마커로 접근 완료', marker_pose=self.target_marker,
                                               drive_status=self.drive_status)
        finally:
            self.cmd_stop_all = False
            self.task_running = False

    async def align_marker(self, start_time=None, as_subtask=True):
        '''마커와의 거리가 근접(close)인 경우에 마커가 로봇 중앙에 오도록 하고,
        마커와의 거리만큼 전진하여 마커가 로봇 중앙에 위치하도록 하고,
        마커의 yaw각도에 맞게 회전하여 정렬을 수행함

        :return marker_id: 로봇 중앙에 있는 마커의 id를 반환함
        '''
        subtask_name = '마커정렬작업'
        if self.task_running:
            yield self.write_navigation_status(time=0, status='error',
                                               error=ERROR.conflict.value, msg=f'{subtask_name} 중복실행')
            raise CmdConflictException()

        self.task_running = True
        if start_time == None:
            start_time = time.time()
        time.sleep(0.5)
        try:
            self.check_stop_all()
            if self.detect_marker() is None:
                self.check_stop_all()
                yield self.write_navigation_status(time=time.time() - start_time, status=subtask_name,
                                                   error=ERROR.marker,
                                                   msg='작업대상 마커가 없습니다.')
                self.task_running = False
                return
            elif self.target_marker.dist != 'close':

                yield self.write_navigation_status(time=time.time() - start_time, status=subtask_name,
                                                   msg='거리가 멀어 마커와 정렬하지 못합니다')
                self.task_running = False
                return

            self.check_stop_all()
            async for event in self.subtask_turn(self.target_marker.theta, start_time=start_time,
                                                 status=subtask_name, msg='마커가 정면에 오도록 회전'):
                self.task_running = False
                yield event

            self.check_stop_all()
            move_dist = self.navigation_param.delta + self.navigation_param.epsilon
            async for event in self.subtask_move(dist=move_dist, start_time=start_time,
                                                 status=subtask_name, msg=f'마커(close)로 접근'):
                self.task_running = False
                yield event

            self.check_stop_all()
            async for event in self.subtask_turn(self.target_marker.yaw, start_time=start_time,
                                                 status=subtask_name, msg='마커와 방향이 일치하도록 회전'):
                self.task_running = False
                yield event

            self.check_stop_all()
            self.marker_action = self.jet_turret.get_marker_action(self.target_marker.marker_id)

            self.task_running = False
            if not as_subtask:
                subtask_name = TASK_STATE.action_complete
            yield self.write_navigation_status(time=time.time() - start_time, status=subtask_name,
                                               msg='마커와 정렬완료', marker_pose=self.target_marker,
                                               drive_status=self.drive_status, marker_action_info=self.marker_action)

        except MarkerServerException as e:
            logger.error(e)
            if as_subtask:
                yield self.write_navigation_status(time=time.time() - start_time, status='error',
                                                   error=ERROR.network.value, msg=f'{e}')
                raise e


        except NoMarkerException as e:
            logger.error(e)
            if as_subtask:
                yield self.write_navigation_status(time=time.time() - start_time, status='error',
                                                   error=ERROR.marker, msg=f'{e}')
                raise e

        except StopByUserException as e:
            logger.error(e)
            if as_subtask:
                yield self.write_navigation_status(time=time.time() - start_time, status=ERROR.stop,
                                                   error=ERROR.stop, msg=f'{e}', marker_pose=self.target_marker,
                                                   drive_status=self.drive_status)
                raise e

        except Exception as e:
            logger.error(e)
            if as_subtask:
                yield self.write_navigation_status(time=time.time() - start_time, status=self.drive_status.error,
                                                   error=self.drive_status.error, msg=f'{e}',
                                                   marker_pose=self.target_marker, drive_status=self.drive_status)
                raise e
        finally:
            self.cmd_stop_all = False
            self.task_running = False

    async def search_marker(self, start_time=None, as_subtask=True):
        ''' 마커가 없을떄 마커를 탐색하는 작업을 수행

        '''
        subtask_name = '마커탐색작업'
        logger.debug('search_marker')
        if self.task_running:
            yield self.write_navigation_status(time=time.time() - start_time, status='error',
                                               error=ERROR.conflict.value, msg=f'마커접근작업 중복실행')
            raise CmdConflictException()

        self.task_running = True

        if start_time == None:
            start_time = time.time()

        try:
            if self.detect_marker():
                if not as_subtask:
                    subtask_name = TASK_STATE.action_complete
                yield self.write_navigation_status(time=time.time() - start_time, status=subtask_name,
                                                   msg='마커탐색 성공', marker_pose=self.target_marker)
                self.task_running = False
                return

            self.check_stop_all()
            async for event in self.subtask_turn(self.navigation_param.theta_s, start_time=start_time,
                                                 status='마커탐색중', msg='왼쪽으로 회전하여 마커탐색'):
                yield event

            self.check_stop_all()
            if self.detect_marker():
                if not as_subtask:
                    subtask_name = TASK_STATE.action_complete
                yield self.write_navigation_status(time=time.time() - start_time, status=subtask_name,
                                                   msg='마커탐색 성공', marker_pose=self.target_marker)
                self.task_running = False
                return

            self.check_stop_all()
            async for event in self.subtask_turn(-2.0 * self.navigation_param.theta_s, start_time=start_time,
                                                 status=subtask_name, msg='오른쪽으로 회전하여 마커탐색'):
                yield event

            self.check_stop_all()
            if self.detect_marker():
                if not as_subtask:
                    subtask_name = TASK_STATE.action_complete
                yield self.write_navigation_status(time=time.time() - start_time, status=subtask_name,
                                                   msg='마커탐색 성공', marker_pose=self.target_marker)
                self.task_running = False
                return

            self.check_stop_all()
            async for event in self.subtask_turn(self.navigation_param.theta_s, start_time=start_time,
                                                 status=subtask_name, msg='정면방향으로 회전'):
                yield event

            self.check_stop_all()
            async for event in self.subtask_move(dist=self.navigation_param.delta, start_time=start_time,
                                                 status=subtask_name, msg=f'{self.navigation_param.delta}cm 만큼 전진'):
                yield event

            self.check_stop_all()
            if self.detect_marker():
                if not as_subtask:
                    subtask_name = TASK_STATE.action_complete
                yield self.write_navigation_status(time=time.time() - start_time, status=subtask_name,
                                                   msg='마커탐색 성공', marker_pose=self.target_marker)
                self.task_running = False
                return
            else:
                self.task_running = False
                raise NoMarkerException()


        except MarkerServerException as e:
            logger.error(e)
            yield self.write_navigation_status(time=time.time() - start_time, status='error',
                                               error=ERROR.network, msg=f'{e}')
            if as_subtask:
                raise e

        except NoMarkerException as e:
            logger.error(e)
            if as_subtask:
                yield self.write_navigation_status(time=time.time() - start_time, status='error',
                                                   error=ERROR.marker, msg=f'{e}')
                raise e


        except StopByUserException as e:
            logger.error(e)
            if as_subtask:
                yield self.write_navigation_status(time=time.time() - start_time, status=ERROR.stop,
                                                   error=ERROR.stop, msg=f'{e}', marker_pose=self.target_marker,
                                                   drive_status=self.drive_status)
                raise e

        except Exception as e:
            logger.error(e)
            if as_subtask:
                yield self.write_navigation_status(time=time.time() - start_time, status=e,
                                                   error=ERROR.unknown, msg=f'{e}',
                                                   marker_pose=self.target_marker,
                                                   drive_status=self.drive_status)
                raise e

        finally:
            self.cmd_stop_all = False
            self.task_running = False

    #################################################################
    ##  내부함수들
    #################################################################
    # def marker_action(self):

    def write_navigation_status(self, time, status, msg, marker_pose: MarkerPose = None,
                                drive_status: DriveStatus = None, error='', marker_action_info=None):
        digit = 2
        dict = {}
        dict['time'] = round(time, digit)
        dict['status'] = status
        dict['msg'] = msg
        dict['error'] = error
        if marker_pose:
            dict['marker_pose'] = marker_pose.model_dump()
        if drive_status:
            dict['drive_status'] = drive_status.model_dump()
        if marker_action_info:
            dict['marker_action_info'] = marker_action_info.model_dump()

        return NavigationStatus(**dict)
        # return json.dumps(dict, ensure_ascii=False) + '\n'

    def detect_marker(self) -> Optional[MarkerPose]:
        logger.info('start detect marker')
        detected_marker = self.marker_detector.get_nearest_marker_pose()
        if detected_marker == None:
            logger.info('marker not detected')
            return None
        elif detected_marker.r > self.navigation_param.max_marker_dist:
            logger.info('marker is too Far')
            return None

        logger.info(detected_marker)



        dict = detected_marker.model_dump()
        logger.debug(dict)
        if dict['r'] > (
                self.navigation_param.min_marker_dist + self.navigation_param.delta + self.navigation_param.epsilon):
            dict['dist'] = 'far'
        elif dict['r'] > self.navigation_param.min_marker_dist + self.navigation_param.epsilon:
            dict['dist'] = 'near'
        else:
            dict['dist'] = 'close'

        self.target_marker = MarkerPose(**dict)
        logger.debug(self.target_marker)
        return self.target_marker

    def check_stop_all(self):
        if self.cmd_stop_all:
            self.cmd_stop_all = False
            raise StopByUserException()


if __name__ == '__main__':
    c = EpidemicRobotController();
