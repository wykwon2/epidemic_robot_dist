class StopByUserException(Exception):
    def __init__(self, msg="사용자에 의한 전체 작업중단"):
        self.msg = msg

    def __str__(self):
        return self.msg


class NoMarkerException(Exception):
    def __init__(self, msg="마커가 인식되지 않았습니다"):
        self.msg = msg

    def __str__(self):
        return self.msg


class MarkerServerException(Exception):
    def __init__(self, msg="카메라 서버가 응답하지 않습니다"):
        self.msg = msg

    def __str__(self):
        return f"{self.msg}"


class ObstacleException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self, msg='장애물 감지로 정지했습니다'):
        return self.msg

class DriveException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self, msg='구동부 오류로 정지했습니다'):
        return self.msg


class CmdConflictException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self, msg='사용자에 의한 정지'):
        return self.msg

