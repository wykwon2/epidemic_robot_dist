import copy, time, math, serial, threading

from logger_config import get_logger

logger = get_logger(__name__)


class LidarPacketHandler:

    def __init__(self, serial_port, baudrate):
        self.serial_io = serial.Serial(serial_port, baudrate=baudrate, parity='N', stopbits=1, bytesize=8, timeout=3)
        self.b_stop_thread = False
        self.b_running_thread = False
        self.read_packet = None

    def run(self):
        self.b_running_thread = True
        while True:
            self.read_packet = self._read_packet()
            #            logger.debug(e)(self.read_packet)
            if self.b_stop_thread == True:
                self.b_stop_thread = False
                break

    def stop(self):
        packet = self._generate_packet([0x02, 0x00])
        self.serial_io.write(packet)
        if self.b_running_thread:
            self.b_stop_thread = True
            self.read_thread.join()

    def request_2d_mode(self):
        self.read_thread = threading.Thread(target=self.run)
        packet = self._generate_packet([0x01, 0x00])
        logger.debug("write=", ''.join(format(x, ' 02x') for x in packet))
        self.serial_io.write(packet)
        received_bytes = self._read_packet()
        self.read_thread.start()
        self.serial_io.reset_input_buffer()
        return received_bytes

    def read_2d_data(self):
        """거리정보 반환 (단위mm)"""
        while self.read_packet is None:
            time.sleep(0.01)
        read_bytes = copy.deepcopy(self.read_packet)
        self.read_packet = None

        data = []
        for theta in range(0, 161):
            r = int.from_bytes(read_bytes[2 * theta:2 * theta + 2], "little")
            degree = -60 + theta * 0.75
            radian = math.radians(degree)
            x = round(r * math.cos(radian), 2)
            y = round(r * math.sin(radian), 2)
            data.append((r, degree, x, y))
        return data

    def get_device_info(self):
        if self.b_running_thread:
            self.b_stop_thread = True
            self.read_thread.join()
        packet = self._generate_packet([0x10, 0x00])
        logger.debug("write=", ''.join(format(x, ' 02x') for x in packet))
        self.serial_io.write(packet)
        received_bytes = self._read_packet()
        logger.debug(received_bytes)
        fw_version = str(int.from_bytes(received_bytes[1:2], 'little')) + '.' \
                     + str(int.from_bytes(received_bytes[2:3], 'little')) + '.' \
                     + str(int.from_bytes(received_bytes[3:4], 'little'))

        hw_version = str(int.from_bytes(received_bytes[4:5], 'little')) + '.' \
                     + str(int.from_bytes(received_bytes[5:6], 'little')) + '.' \
                     + str(int.from_bytes(received_bytes[6:7], 'little'))

        return {"fw_version": fw_version, "hw_version": hw_version}

    def set_baudrate(self, baud_str: str):
        bps = {'57600': 0x39, '115200': 0xAA, '250000': 0x77, '3000000': 0x55}
        value = bps[baud_str]
        if value is None:
            return None
        packet = self._generate_packet([0x12, value])
        self.serial_io.write(packet)
        # no receive packet

    def _generate_packet(self, payload: list):
        #   add packet length
        length = len(payload)
        packet = [length & 0xff, length >> 8 & 0xff] + payload
        # add checksum
        checksum = 0
        for v in packet:
            checksum ^= int(v)
        packet.append(checksum)
        # add header
        return bytes([0x5a, 0x77, 0xff] + packet)

    def _read_packet(self):
        self.serial_io.read_until(b'\x5a\x77\xff')
        while True:
            # 패킷 길이 분석
            length_bytes = self.serial_io.read(2)
            length = int.from_bytes(length_bytes, "little")
            body_bytes = self.serial_io.read(length)
            checksum_bytes = self.serial_io.read()
            received_checksum = int.from_bytes(checksum_bytes, "little")
            calculated_checksum = 0
            for v in length_bytes + body_bytes:
                calculated_checksum ^= int(v)

            if received_checksum != calculated_checksum:
                break
            # body_bytes 에서 checksum을 제외
            return body_bytes


if __name__ == "__main__":
    fig, ax = plt.subplots()
    x, y = [], []
    sc = ax.scatter(x, y)
    ax.grid(True)
    handler = LidarPacketHandler(serial_port="COM3", baudrate="57600")
    ret = handler.get_device_info()
    ret = handler.request_2d_mode()
    data = handler.read_2d_data()
    for p in data:
        x.append(p[0])
        y.append(p[1])
    ax.plot(x, y)

    for i in range(0, 1):
        ax.show()

    handler.stop()
