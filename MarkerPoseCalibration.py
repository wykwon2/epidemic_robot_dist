import math, cv2, logging
import numpy as np

import ConfigManager
import calibration_util as utils

from dataclass import MarkerPose
from logger_config import get_logger



logger = get_logger(__name__)


class MarkerPoseCalibration:
    mtx = None
    dist = None
    rvec_registered = None
    tvec_registered = None
    marker_id_registered = None
    camera_id = None
    offset_xy = None
    marker_length = None

    def __init__(self, config: ConfigManager):
        self.config = config
        self.dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_250)
        self.parameters = cv2.aruco.DetectorParameters_create()
        dict = config.calibration_cam
        self.mtx = np.array(dict['camera_matrix'])
        self.dist = np.array(dict['dist_coeff'])
        self.marker_id_registered = dict['marker_id_registered']
        self.offset_xy = np.array(dict["offset_xy"])
        self.marker_length = dict["marker_length"]
        rvec = dict['rvec_registered']
        if rvec is not None:
            self.rvec_registered = np.array(rvec).reshape(1, 3)
        tvec = dict['tvec_registered']
        if tvec is not None:
            self.tvec_registered = np.array(tvec).reshape(1, 3)

    def register_marker(self, image, marker_id, offset_x, offset_y, marker_length):
        """로봇의 정해진 위치에 마커를 두고 마커와 로봇등의 카메라 장착물체와의 상대거리로부터 얻어진 rvec, tvec등을 저장한다.
        이후 등록 마커와 관측된 마커, 그리고 offset을 이용해 마커의 3차원 좌표를 알아낸다
        :param image 촬영 영상
        :param marker_id 등록할 마커 id
        :param offset_x 로봇과 마커간의 상대위치 단위[cm]
        :param offset_y 로봇과 마커간의 상대위치 단위[cm]
        :param marker_length 마커의 한쪽면 길이 [cm]
        """
        self.offset_xy = [offset_x, offset_y]
        self.marker_id_registered = marker_id
        self.marker_length = marker_length
        corners, ids, rejected = cv2.aruco.detectMarkers(image, self.dictionary, parameters=self.parameters)

        if len(corners) > 0:
            for (markerCorner, markerID) in zip(corners, ids):
                if markerID == self.marker_id_registered:
                    corners = markerCorner.reshape((1, 4, 2))
                    self.rvec_registered, self.tvec_registered, _ = cv2.aruco.estimatePoseSingleMarkers(corners,
                                                                                                        self.marker_length,
                                                                                                        self.mtx,
                                                                                                        self.dist)

                    logger.debug("id=", markerID, "=", self.rvec_registered, self.tvec_registered)
                    self.save_conf()
                    break

    def save_conf(self):
        dict = {'camera_id': self.camera_id,
                'camera_matrix': np.asarray(self.mtx).tolist(),
                'dist_coeff': np.asarray(self.dist).tolist(),
                'rvec_registered': np.asarray(self.rvec_registered).tolist(),
                'tvec_registered': np.asarray(self.tvec_registered).tolist(),
                'offset_xy': self.offset_xy,
                'marker_id_registered': self.marker_id_registered,
                'marker_length': self.marker_length,
                }
        self.config.set_calibration_cam(dict)

    def calibrate(self, image) -> [MarkerPose]:
        """ 영상에 있는 마커의 자세를 등록된 기준마커에 대한 상대위치로서 반환한다.
            화면좌표계와
         """
        corners, ids, rejected = cv2.aruco.detectMarkers(image, self.dictionary, parameters=self.parameters)
        marker_pose_list = []
        if len(corners) > 0:
            for (markerCorner, markerID) in zip(corners, ids):
                corners = markerCorner.reshape((1, 4, 2))
                rvec_cur, tvec_cur, _ = cv2.aruco.estimatePoseSingleMarkers(corners, self.marker_length, self.mtx,
                                                                            self.dist)
                #                logger.debug(e)("id=", markerID, "=", self.rvec_registered, self.tvec_registered)
                rvec_relative, tvec_relative = utils.relativePosition(rvec_cur, tvec_cur,
                                                                      self.rvec_registered,
                                                                      self.tvec_registered)
                rpy = utils.Rodrigues_to_Euler(rvec_relative)
                dx = tvec_relative[0][0] + self.offset_xy[0]
                dy = tvec_relative[1][0] + self.offset_xy[1]
                yaw = np.rad2deg(rpy[2])
                marker_pose = {}
                marker_pose['marker_id'] = markerID
                marker_pose['dist'] = 'unknown'
                marker_pose['x'] = round(dx, 2)
                marker_pose['y'] = round(dy, 2)
                marker_pose['r'] = round(math.sqrt(dx * dx + dy * dy), 2)
                marker_pose['theta'] = round(np.rad2deg(math.atan2(dy, dx)), 2)
                marker_pose['yaw'] = round(yaw, 2)
                marker_pose = MarkerPose(**marker_pose)
                marker_pose_list.append(marker_pose)

        #                logger.debug(e)(f"dx={dx}, dy={dy}, yaw={yaw}")
        return marker_pose_list

    def get_nearest_marker(self, image):
        marker_pose_list = self.calibrate(image)
        min_dist = float("inf")
        nearest_marker_pose = None
        for marker_pose in marker_pose_list:
            distance = math.sqrt(marker_pose.x * marker_pose.x + marker_pose.y * marker_pose.y)
            if distance < min_dist:
                nearest_marker_pose = marker_pose
                min_dist = distance
        return nearest_marker_pose


if __name__ == "__main__":
    c = MarkerPoseCalibration(0)
    camera = cv2.VideoCapture(0)
    camera.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
    camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
    ret, img = camera.read()
    #    c.register_marker(img, marker_id=2, offset_x=60, offset_y=0, marker_length=5)
    #    result = c.calibrate(img)
    #    logger.debug(e)(result)
    logger.debug(c.get_nearest_marker(img))
    #    cv2.imshow("img", img)
