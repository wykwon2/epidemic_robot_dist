import asyncio
import json
import logging
import time
import copy
from collections import deque

import serial
import threading
import numpy as np
import yaml

from ConfigManager import ConfigManager
from LidarController import LidarController
from dataclass import DeviceStatus, DriveParam, BATTERY, DriveStatus, ERROR
from user_exceptions import StopByUserException, ObstacleException, CmdConflictException

logger = logging.getLogger(__name__)


class DiffDriveController:
    cmd_deque = deque()
    controller_status={}
    sensing_inteval = 0.025
    control_interval_div = 3
    max_obstruction_count = 50
    obstruction_points = 20
    serial_thread = None
    drive_running = False
    cmd_stop_all = False
    drive_param: DriveParam = None
    max_l_speed = 20
    max_a_speed = 50
    sim_dt = 0.1

    def __init__(self, drive_serial_port: str, lidar_serial_port, param: DriveParam, simulation=False):

        self.simulation = simulation
        if not self.simulation:
            self.drive_serial_io = serial.Serial(drive_serial_port, baudrate=57600, parity='N', stopbits=1, bytesize=8,
                                                 timeout=8)
            logger.info(f"diff drive connected to {drive_serial_port}")
        if not self.simulation:
            self.lidar = LidarController(lidar_serial_port, baudrate="57600", simulation=simulation)

        if self.simulation:
            self.controller_status = {'time': 0, 'g_x': 0, 'g_y': 0, 'g_heading': 0, 'status': '', 'target': '',
                                      'l_x': 0, 'l_y': 0, 'l_heading': 0, 'l_vel': 0, 'a_vel': 0, 'travel': 0,
                                      'battery': 1000, 'lidar_points': 0, 'obstruction_cnt': 0}
        time.sleep(4)
        self._start_sensing()
        self.set_param(param)

        self.serial_thread = threading.Thread(target=self.serial_io_thread)
        self.serial_thread.start()


        logger.info("start sensing in diffdrive")

    def stop_all(self):
        self.cmd_stop_all = True

    def set_param(self, param: DriveParam):
        self.drive_param = param
        self._set_sensing_interval(param.sensing_interval)

    def get_lidar(self):
        return self.lidar

    def restart(self):
        self.b_stop = True
        self.serial_thread.join()
        self.serial_thread = threading.Thread(target=self.run)
        self.serial_thread.start()

    def get_status(self):
        status = copy.deepcopy(self.controller_status)
        return status

    def get_battery_raw_value(self):
        if self.controller_status:
            return self.controller_status['battery']

    def get_battery_level(self):
        if self.controller_status:
            v = self.controller_status['battery']
            if v > self.drive_param.battery_good_value:
                return BATTERY.high
            elif v > self.drive_param.battery_normal_value:
                return BATTERY.mid
            else:
                return BATTERY.low

    def get_error_msg(self):
        return self.error_msg

    async def move(self, distance: float, l_speed=max_l_speed):
        if self.drive_running:
            yield self.write_drive_status(status='error', time=0, target=distance, travel=0, msg='구동부 가동중에 새로운 구동명령 받음',
                                          error=ERROR.conflict)
            raise CmdConflictException()

        else:
            self.drive_running = True

        try:
            start_time = time.time()
            sensing_inteval = 0.01
            threshold = 2 * (0.02 * l_speed);
            if distance >= 0:
                l_speed = l_speed * 0.01;
            else:
                l_speed = -l_speed * 0.01;

            if self.simulation:
                self.controller_status['l_x'] = 0
            if self.drive_param.use_lidar:
                yield self.write_drive_status(status='move', time=time.time() - start_time, target=distance, travel=0,
                                              msg='before lidar start sending')
                logger.info('before lidar start sending')
                if not self.simulation:
                    self.lidar.start_sensing()
                yield self.write_drive_status(status='move', time=time.time() - start_time, target=distance, travel=0,
                                              msg='after lidar start sending')
                logger.info('after lidar start sending')
            x0 = self.controller_status['l_x'] * 100.0
            cmd_cnt = 0
            obstruction_count = 0
            travel_dist = 0
            lidar_points = 0
            while True:
                if self.cmd_stop_all:
                    raise StopByUserException()
                if self.drive_param.use_lidar and not self.simulation:
                    lidar_points = self.lidar.detect_obstruction()
                    if (lidar_points > self.obstruction_points) and (distance > 0):
                        obstruction_count += 1
                        yield self.write_drive_status(status='move', time=time.time() - start_time, target=distance,
                                                      travel=travel_dist, msg='장애물 감지에 따른 일시정지',
                                                      lidar_points=lidar_points, obstruction_cnt=obstruction_count)
                        self.stop()
                    # control_interval_div 만큼
                    elif cmd_cnt % self.control_interval_div == 0:
                        if not self.simulation:
                            self.cmd_deque.append(f"setvel {l_speed} {0}\n")

                        yield self.write_drive_status(status='move', time=time.time() - start_time, target=distance,
                                                      travel=travel_dist, msg='정상동작',
                                                      lidar_points=lidar_points, obstruction_cnt=obstruction_count)

                elif cmd_cnt % self.control_interval_div == 0:
                    if not self.simulation:
                        self.cmd_deque.append(f"setvel {l_speed} {0}\n")
                    yield self.write_drive_status(status='move', time=time.time() - start_time, target=distance,
                                                  travel=travel_dist, msg='정상동작',
                                                  lidar_points=lidar_points, obstruction_cnt=obstruction_count)

                if self.simulation:
                    self.controller_status['l_x'] += self.sim_dt * l_speed
                    yield self.write_drive_status(status='move', time=time.time() - start_time, target=distance,
                                                  travel=travel_dist, msg='정상동작',
                                                  lidar_points=lidar_points, obstruction_cnt=obstruction_count)

                if self.drive_param.use_lidar and (obstruction_count > self.max_obstruction_count):
                    raise ObstacleException()

                cmd_cnt += 1
                time.sleep(sensing_inteval)
                x = self.controller_status['l_x'] * 100.0
                travel_dist = abs(x - x0)
                if (abs(distance) - travel_dist) < threshold:
                    break
                if abs(travel_dist) > abs(distance):
                    break

        except StopByUserException as e:
            self.cmd_stop_all = False
            yield self.write_drive_status(status='error', time=time.time() - start_time, target=distance,
                                          travel=travel_dist, msg='사용자 정지에 따른 중단',
                                          error=ERROR.stop, lidar_points=lidar_points,
                                          obstruction_cnt=obstruction_count)
        except ObstacleException as e:
            yield self.write_drive_status(status='error', time=time.time() - start_time, target=distance,
                                          travel=travel_dist, msg='장애물 감지로 인한 작업중단',
                                          error=ERROR.obstacle, lidar_points=lidar_points,
                                          obstruction_cnt=obstruction_count)
        else:
            yield self.write_drive_status(status='ready', time=time.time() - start_time, target=distance,
                                          travel=travel_dist, msg='이동종료',
                                          lidar_points=lidar_points, obstruction_cnt=obstruction_count)
        finally:
            self.stop()
            if self.drive_param.use_lidar and not self.simulation:
                self.lidar.stop_sensing()
            self.cmd_deque.append("reset local\n")
            self.drive_running = False







    async def turn(self, degree: float, a_speed=max_a_speed):
        if self.drive_running:
            yield self.write_drive_status(status='error', time=0, target=degree, travel=0, msg='구동부 가동중에 새로운 구동명령 받음',
                                          error=ERROR.conflict)
            raise CmdConflictException()
        else:
            self.drive_running = True

        try:
            if self.simulation:
                self.controller_status['l_heading'] = 0

            start_time = time.time()
            threshold = 2 * (0.02 * a_speed);
            threshold = 1;
            if degree >= 0:
                a_speed = np.deg2rad(a_speed);
            else:
                a_speed = -np.deg2rad(a_speed);
            travel_angle = 0
            heading0 = np.rad2deg(self.controller_status['l_heading'])

            cmd_cnt = 0
            while True:
                if self.cmd_stop_all:
                    raise StopByUserException()

                if cmd_cnt % self.control_interval_div == 0:
                    self.cmd_deque.append(f"setvel {0} {a_speed}\n")
                    yield self.write_drive_status(status='turn', time=time.time() - start_time, target=degree,
                                                  travel=travel_angle, msg='Turn 작업중 ')

                if self.simulation:
                    self.controller_status['l_heading'] += 0.5 * self.sim_dt * a_speed

                cmd_cnt = cmd_cnt + 1
                time.sleep(self.sensing_inteval)
                heading = np.rad2deg(self.controller_status['l_heading'])
                travel_angle = abs(heading - heading0)

                if (abs(degree) - travel_angle) < threshold:
                    break
                if abs(travel_angle) > abs(degree):
                    break

        except StopByUserException as e:
            self.cmd_stop_all = False
            yield self.write_drive_status(status='error', time=time.time() - start_time, target=degree,
                                          travel=travel_angle, msg='사용자 정지에 따른 중단',
                                          error=ERROR.stop)
        except ObstacleException as e:
            yield self.write_drive_status(status='error', time=time.time() - start_time, target=degree,
                                          travel=travel_angle, msg='장애물 감지로 인한 작업중단',
                                          error=ERROR.obstacle)
        else:
            yield self.write_drive_status(status='ready', time=time.time() - start_time, target=degree,
                                          travel=travel_angle, msg='이동종료')

        finally:
            self.stop()
            self.drive_running = False


    def write_drive_status(self, time, status, target, travel, lidar_points=0,
                           obstruction_cnt=0, error="", msg=""):
        digit = 2
        self.controller_status['time'] = round(time, digit)
        self.controller_status['status'] = status
        self.controller_status['target'] = round(target, digit)
        self.controller_status['travel'] = round(travel, digit)
        if lidar_points > 0:
            self.controller_status['lidar_points'] = lidar_points
        if obstruction_cnt > 0:
            self.controller_status['obstruction_cnt'] = obstruction_cnt
        if error is not None:
            self.controller_status['error'] = error
        if msg is not None:
            self.controller_status['msg'] = msg

        return DriveStatus(**self.controller_status)


    def stop(self):
        self.cmd_deque.clear()
        self.cmd_deque.append("setvel 0 0\n")
        self.cmd_deque.append("reset local\n")

    def _start_sensing(self):
        self.cmd_deque.append("start-sensing\n")

    def _stop_sensing(self):
        self.cmd_deque.append("stop-sensing\n")

    def _set_sensing_interval(self, interval_sec:float):
        self.sensing_inteval=interval_sec
        interval_ms = int(interval_sec*1000)
        self.cmd_deque.append(f"interval {interval_ms}\n")

    def serial_io_thread(self):
        logger.info("serial io thread start")
        while True:
            if len(self.cmd_deque) > 0:
                cmd: str = self.cmd_deque.popleft()
                if not self.simulation:
                    logger.info(f'cmd_recv {cmd}')
                    self.drive_serial_io.write(cmd.encode())

            if not self.simulation:
                try:
                    message = self.drive_serial_io.readline()
                    message = message.decode('utf-8').strip()
                    packet = message.split(",")

                    if len(packet) == 9:
                        self.controller_status['g_x'] = float(packet[0])
                        self.controller_status['g_y'] = float(packet[1])
                        self.controller_status['g_heading'] = float(packet[2])
                        self.controller_status['l_x'] = float(packet[3])
                        self.controller_status['l_y'] = float(packet[4])
                        self.controller_status['l_heading'] = float(packet[5])
                        self.controller_status['l_vel'] = float(packet[6])
                        self.controller_status['a_vel'] = float(packet[7])
                        self.controller_status['battery'] = float(packet[8])
                        self.controller_status['error'] = ""
                        # logger.info(self.controller_status)
                    else:
                        logger.info(f"in diff drive controller: wrong packet {message} size={len(packet)}")

                except Exception as e:
                    logger.error(e)
                    raise e


    # drive = DiffDriveController(drive_serial_port="/dev/ttyCH341USB1", lidar_serial_port="/dev/serial0")
    # drive = DiffDriveController(drive_serial_port="/dev/ttyUSB1", lidar_serial_port="/dev/serial0")



if __name__ == "__main__":
    config = ConfigManager()
    config.load_config()
    drive = DiffDriveController(drive_serial_port="COM4", lidar_serial_port="COM6", param=config.drive_params)
    logger.info("init")
    drive.turn(50)
    #    drive.move(10, 22)
    #    drive.turn(30, 30)
    #    drive.turn(-30, 30)
    while True:
        time.sleep(0.1)